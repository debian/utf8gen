/*
   utf8gen - convert hexadecimal input to UTF-8 numbers

   Author: Paul Hardy

   Date: June 2018

   Synopsis: utf8gen [ [-e <format1>] | [-E <format2>] ] [-r <formatr>]
                     [ [-u <utf8_format>] | -n]
                     [-c] [-s]
                     [-i <input_file>] [-o <output_file>]


   Author: Paul Hardy, unifoundry <at> unifoundry.com, June 2018
   
   
   Copyright (C) 2018 Paul Hardy

   LICENSE:

      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>  /* created by autotools */

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

/* To check functionality for compiling on GNU-based systems */
#define _GNU_SOURCE

/* These two defines are for diagnostic & help output */
#ifdef PACKAGE_NAME
#define PROG_NAME PACKAGE_NAME
#else
#define PROG_NAME "utf8gen"
#endif

#ifdef PACKAGE_VERSION
#define PROG_VERSION PACKAGE_VERSION
#else
#define PROG_VERSION "1.0"
#endif

#define MAXSTRING 4098    /* maximum number of characters on an input line */

/* For handling errors in system functions */
extern int errno;


int
main (int argc, char *argv[])
{
   int i;              /* loop variable                         */
   int in_formats=0;   /* number of times to print input number */
   uint32_t codept;    /* Unicode code point to convert         */
   char instring[MAXSTRING]; /* input line                      */
   unsigned utf8_bytes[5]; /* encoded UTF-8 bytes, ending with null byte */
   int print_remainder=0; /* =1 to print input string following code point */
   int print_char=0;   /* =1 to end output line with <space>+UTF-8 character */
   int swap_order=0;   /* =1 to print UTF-8 first, then input format(s) */
   int print_codes=1;  /* print UTF-8 encoding; don't print if == 0     */
   int exit_status;    /* program exit status                           */

   /*
      Format strings for printing input number and output UTF-8.
      By default, do not print the input code point, but print the
      output UTF-8 character using the default_out format string.
   */
   static char *default_out = "\\%03o";   /* default output UTF-8 format */
   char *in_format="";             /* format to print input  number */
   char *rem_format="";            /* format for input remainder    */
   char *out_format = default_out; /* format to print output number */

   void fatal_error (int, char *);
   void print_help  ();
   int  cvt2utf8    (uint32_t, unsigned *);
   void fprint_utf8 (FILE *, unsigned *, char *);
   void print_instring  (int, int, char *, int, char *, char *, FILE *);
   void print_outstring (int, int, unsigned *, char *, FILE *);

   int  interactive=1;    /* =1 if reading from terminal, 0 otherwise  */
   FILE *infp  = stdin;   /* input  file pointer; default is stdin  */
   FILE *outfp = stdout;  /* output file pointer; default is stdout */

   exit_status = EXIT_SUCCESS;

   interactive = isatty (fileno (stdin)) ? 1 : 0;

   for (i = 1; i < argc; i++) {
      /*
         Parse options.  If an invalid command line argument
         was given, print a help menu and exit with error status.
      */
      if (argv[i][0] == '-' && exit_status == EXIT_SUCCESS) {
         switch (argv[i][1]) {
            /* Echo input number one way before printing conversion (-e) */
            case 'e':
                 if (++i < argc) {
                    in_formats = 1;
                    in_format = argv[i];
                 }
                 else {
                    fatal_error (interactive,
                                 "Missing echo format string after -e");
                 }
                 break;
            /* Echo input number two ways before printing conversion (-E) */
            case 'E':
                 if (++i < argc) {
                    in_formats = 2;
                    in_format = argv[i];
                 }
                 else {
                    fatal_error (interactive,
                                 "Missing echo format string pair after -E");
                 }
                 break;
            /*
               print remaining string that followed
               the hexadecimal Unicode code point (-r)
            */
            case 'r':
                 if (++i < argc) {
                    print_remainder = 1;
                    rem_format = argv[i];
                 }
                 else {
                    fatal_error (interactive,
                                 "Missing remainder of string format after -r");
                 }
                 break;
            /* UTF-8 output format for each encoded byte (-u) */
            case 'u':
                 if (++i < argc) {
                    out_format = argv[i];
                 }
                 else {
                    fatal_error (interactive,
                                 "Missing format string after '-u'");
                 }
                 break;
            /* do not print the UTF-8 byte codes (-n) */
            case 'n':
               print_codes = 0;
               break;
            /* end line by printing <space> + UTF-8 character (-c) */
            case 'c':
               print_char = 1;
               break;
            /* swap output order: print UTF-8 components, then input (-s) */
            case 's':
               swap_order = 1;
               break;
            /* input filename (-i) */
            case 'i':
                 if (++i < argc) {
                    infp = fopen (argv[i], "r");
                    if (infp == NULL) {
                       fprintf (stderr,
                                "%s: cannot open %s for input - %s\n\n",
                                PROG_NAME, argv[i], strerror (errno));
                       exit (EXIT_FAILURE);
                    }
                 }
                 else {
                    fatal_error (interactive,
                                 "No input filename give after '-i'");
                 }
                 break;
            /* output filename (-o) */
            case 'o':
                 if (++i < argc) {
                    outfp = fopen (argv[i], "w");
                    if (outfp == NULL) {
                       fprintf (stderr,
                                "%s: cannot open %s for output - %s\n\n",
                                PROG_NAME, argv[i], strerror (errno));
                       exit (EXIT_FAILURE);
                    }
                 }
                 else {
                    fatal_error (interactive,
                                 "No output filename give after '-o'");
                 }
                 break;
            /* Print help message (-h, -?) */
            case 'h':
            case '?':
                 print_help ();
                 exit (EXIT_SUCCESS);
                 break;
            /* Option starts with "--"; look for "--help" and "--verbose" */
            case '-':
                 /* (--help) */
                 if (strcmp (&argv[i][2], "help") == 0) {
                    print_help ();
                    exit (EXIT_SUCCESS);
                 }
                 /* (--version) */
                 else if (strcmp (&argv[i][2], "version") == 0) {
                    printf ("%s %s\n", PROG_NAME, PROG_VERSION);
                    printf ("Copyright (C) 2018 Paul Hardy\n");
                    printf ("License GPLv2+: GNU GPL version 2 or later <http://gnu.org/licenses/gpl.html>\n");
                    printf ("This is free software: you are free to change and redistribute it.\n");
                    printf ("There is NO WARRANTY, to the extent permitted by law.\n\n");
                    exit (EXIT_SUCCESS);
                 }
                 else {
                    fatal_error (interactive, "Unrecognized option");
                 }
                 break;
            default:
                 fatal_error (interactive, "Unrecognized option");
                 break;
         }
      }
      else {
         if (infp == stdin)
            fprintf (stderr, "Unrecognized parameter %s\n\n",
                     argv[i]);
         else
            fprintf (stderr, "%s: unrecognized parameter %s\n\n",
                     PROG_NAME, argv[i]);

         print_help ();
         exit (EXIT_FAILURE);
      }
   }

   /*
      Read one number per input line, possibly with following string
   */
   codept = 0;  /* Initialize to avoid blank line input */
   while (fgets (instring, MAXSTRING, infp) != NULL) {
      /* Get Unicode code point at start of line */
      if (instring[0] != '\n' && instring[0] != '\0') {
         sscanf (instring, "%X", &codept);

         if (cvt2utf8 (codept, utf8_bytes) > 0) {  /* If in Unicode range */
            /*
               Non-swapped output (no option '-s'); echo input line first
            */
            if (swap_order == 0) {  /* Print input values first */
               print_instring (codept, in_formats,in_format,
                               print_remainder, rem_format,
                               instring, outfp);
            }  /* swap_order == 0 */

            /*
               Print selected UTF-8 encoding output and/or the character itself
            */
            print_outstring (print_char, print_codes, utf8_bytes, out_format, outfp);

            /*
               Swapped output (option '-s'); echo input line after other output
            */
            if (swap_order == 1) {  /* Print input values last */
               print_instring (codept, in_formats,in_format,
                               print_remainder, rem_format,
                               instring, outfp);
            }  /* swap_order == 1 */

            fprintf (outfp, "\n");  /* Printed all output for this input line */

         }  /* cvt2utf8 (codept, utf8_bytes) >= 0 */
         else {
            if (interactive) {  /* Print error, but keep going */
               fprintf (stderr, "Out of range Unicode value > 10FFFF\n");
            }
            else {  /* Non-interactive -- abort */
               fatal_error (interactive,
                            "Out of range Unicode value > 10FFFF");
            }
         }  /* cvt2utf8 (codept, utf8_bytes) < 0 (invalid code point) */
         codept = 0;  /* reset to zero in case next input is a blank line */
      }  /* input line did not start with a newline or '\0' */
   }  /* while not at end of input */

   fclose (outfp);

   exit (exit_status);
}


/*
   Print an error message, print the help menu, then quit
   with non-zero exit status.

   If the input file pointer points to stdin, do not print
   the program naem and begin the message with an uppercase
   letter.  Otherwise, print the program name and begin the
   message with a lowercase letter.
*/
void
fatal_error (int interactive, char *err_message)
{
   void print_help ();

   if (interactive) {
      fprintf (stderr, "%c%s\n\n",
               toupper (err_message[0]), &err_message[1]);
   }
   else {
      fprintf (stderr, "%s: %c%s\n\n",
               PROG_NAME, tolower (err_message[0]), &err_message[1]);
   }

   if (interactive) print_help ();

   exit (EXIT_FAILURE);
}


/*
   Print a help message.

   If the input file pointer points to stdin, do not print
   the program naem and begin the message with an uppercase
   letter.  Otherwise, print the program name and begin the
   message with a lowercase letter.
*/
void
print_help ()
{

   fprintf (stdout, "Syntax: %s { [-e <format1>] | [-E <format2>] } ",
            PROG_NAME);
   fprintf (stdout, "[-r <formatr>]\n");
   fprintf (stdout, "                [ [-u <utf8_format>] | -n] [-c] [-s]\n");
   fprintf (stdout, "                [-i <input_file>] [-o <output_file>]\n\n");
   fprintf (stdout, "        <format1>, <format2>, <formatr>, and <utf8_format>\n");
   fprintf (stdout, "        are printf format strings\n\n");
   fprintf (stdout, "        -e         Echo input code point in one format\n\n");
   fprintf (stdout, "        -E         Echo input code point in two formats\n\n");
   fprintf (stdout, "        -r         Print remainder of input after code point\n\n");
   fprintf (stdout, "        -u         UTF-8 output format\n\n");
   fprintf (stdout, "        -n         Do not print UTF-8 codes\n\n");
   fprintf (stdout, "        -c         print <space>+UTF-8 character after UTF-8 bytes\n\n");
   fprintf (stdout, "        -s         Swap order: print UTF-8 string first, then input value\n\n");
   fprintf (stdout, "        -h\n");
   fprintf (stdout, "        --help     This help message\n\n");
   fprintf (stdout, "        --version  Program version information\n\n");

   fprintf (stdout, "     Examples:\n\n");
   fprintf (stdout, "         %s -e \"0x%%04X \" -u \"\\%%03o\"\n\n",
            PROG_NAME);
   fprintf (stdout, "         %s -E \"U+%%04x = 0%%02o = \"\n\n",
            PROG_NAME);
   fprintf (stdout, "         %s -s -e \"  /* U+%%04X */\" -u \"\\%%03o\"\n\n",
            PROG_NAME);
   fprintf (stdout, "     Valid Unicode values range from hexadecimal 0 through 10FFFF\n\n");

   return;
}


/*
   Convert a Unicode code point to a UTF-8 string.
   The allowable Unicode range is U+0000..U+10FFFF.

   codept     - the Unicode code point to encode
   utf8_bytes - an array of 5 bytes to hold the UTF-8 encoded string;
                the string will consist of up to 4 UTF-8-encoded bytes,
                with null bytes after the last encoded byte to signal
                to the end of the array, utf8_bytes[4].
*/
int
cvt2utf8 (uint32_t codept, unsigned *utf8_bytes)
{
   int bin_length;     /* number of binary digits, for forming UTF-8 */
   int byte_length;    /* numberof bytes of UTF-8                    */

   int bin_digits (uint32_t);


   /*
      If codept is within the valid Unicode range of
      0x0 through 0x10FFFF inclusive, convert it to UTF-8.
   */
   if (codept <= 0x10FFFF) {

      byte_length = 0;
      bin_length  = bin_digits (codept);

      if (bin_length < 8) {        /* U+0000..U+007F */
         byte_length = 1;
         utf8_bytes [0] = codept;
         utf8_bytes [1] =
         utf8_bytes [2] =
         utf8_bytes [3] =
         utf8_bytes [4] = 0;
      }
      else if (bin_length < 12) {  /* U+0080..U+07FF */
         byte_length = 2;
         utf8_bytes [0] = 0xC0 | ((codept >>  6) & 0x1F);
         utf8_bytes [1] = 0x80 | ( codept        & 0x3F);
         utf8_bytes [2] =
         utf8_bytes [3] =
         utf8_bytes [4] = 0;
      }
      else if (bin_length < 17) {  /* U+0800..U+FFFF */
         byte_length = 3;
         utf8_bytes [0] = 0xE0 | ((codept >> 12) & 0x0F);
         utf8_bytes [1] = 0x80 | ((codept >>  6) & 0x3F);
         utf8_bytes [2] = 0x80 | ( codept        & 0x3F);
         utf8_bytes [3] =
         utf8_bytes [4] = 0;
      }
      else if (bin_length < 22) {  /* U+010000..U+10FFFF */
         byte_length = 4;
         utf8_bytes [0] = 0xF0 | ((codept >> 18) & 0x07);
         utf8_bytes [1] = 0x80 | ((codept >> 12) & 0x3F);
         utf8_bytes [2] = 0x80 | ((codept >>  6) & 0x3F);
         utf8_bytes [3] = 0x80 | ( codept        & 0x3F);
         utf8_bytes [4] = 0;
      }

   }  /* encoded output for valid Unicode code point */
   else { /* flag out of range Unicode code point */
      /*
         0xFF is never a valid UTF-8 code point, so testing
         for it will be an easy check of a valid return value.
      */
      byte_length = -1;
      utf8_bytes [0] = 0xFF;
      utf8_bytes [1] = 0xFF;
      utf8_bytes [2] = 0xFF;
      utf8_bytes [3] = 0xFF;
      utf8_bytes [4] = 0;
   }

   return byte_length;
}


/*
   Print an array of bytes comprising one UTF-8 encoded character.

   outfp      - the output stream file pointer
   utf8_bytes - an array of 7 bytes holding a null-terminated UTF-8 string
   utf_format - format for fprintf to use with each byte
*/
void
fprint_utf8 (FILE *outfp,
                  unsigned *utf8_bytes,
                  char *utf_format)
{

   int i;  /* loop variable */

   for (i = 0; utf8_bytes[i] != 0x00; i++)
      fprintf (outfp, utf_format, utf8_bytes[i] & 0xFF);

   return;
}


/*
   Return the number of significant binary digits in an unsigned number.
*/
int
bin_digits (uint32_t itest)
{
   uint32_t i;
   int result;

   i = 0x80000000;  /* mask highest uint32_t bit */
   result = 32;
   while (  (i != 0) && ((itest & i) == 0) ) {
       i >>= 1;
       result--;
   }

   return result;
}


/*
   Output the input line in the desired format.

   codept           The Unicode code point

   in_formats       Number of ways to format the input
                    code point (1 or 2)

   in_format        The format string to use to print the
                    code point

   print_remainder  1 if printing remainder of input string,
                    0 otherwise

   rem_format       The format string for the remainder of input
                    following the code point + space

   instring         The entire line of input, null terminated

   outfp            The output file pointer
*/
void
print_instring (int codept,
                int in_formats,      char *in_format,
                int print_remainder, char *rem_format,
                char *instring,      FILE *outfp)
{
   int i;  /* loop variable */


   if (in_formats == 1)  /* option '-e' specified */
      fprintf (outfp, in_format, codept);
   else if (in_formats == 2)  /* option '-E' specified */
      fprintf (outfp, in_format, codept, codept);

   if (print_remainder == 1) {  /* option '-r' specified */
      instring [strlen (instring) - 1] = '\0';
      /* Find start of hexadecimal number */
      for (i = 0;
           isspace (instring[i]) || instring[i] == '\0';
           i++);
      if (instring[i] != '\0') {
         /* Find space after hexadecimal number */
         for (i = 0;
              !isspace (instring[i]) || instring[i] == '\0';
              i++);

         i++;  /* Skip the space character */
         /* If a valid string, print it */
         if (i < strlen (instring) && instring[i] != '\0') {
            fprintf (outfp, rem_format, &instring[i]);
         }
      }  /* instring[i] != '\0' */
   }  /* print_remainder == 1 */


   return;
}


/*
   Print UTF-8 encoded output string and/or UTF-8 character

   print_char   1 to print the Unicode character itself as
                a UTF-8 byte stream

   print_codes  1 to print UTF-8 encoded bytes numerically

   utf8_bytes   The byte string of UTF-8 values, null-terminated

   out_format   The output format to use for printing UTF-8 bytes

   outfp        The output file pointer
*/
void
print_outstring (int print_char, int print_codes,
                 unsigned *utf8_bytes,
                 char *out_format, FILE *outfp)
{
   int i;  /* loop variable */

   void fprint_utf8 (FILE *, unsigned *, char *);

   if (print_codes == 1) {  /* no option '-n' specified */
      fprint_utf8 (outfp, utf8_bytes, out_format);
   }
   if (print_char == 1) {  /* option '-c' */
      fputc (' ', outfp);
      for (i = 0; i < 4 && utf8_bytes[i] != '\0'; i++)
         fputc (utf8_bytes[i], outfp);
   }

   return;
}

