\056 . -- Full Stop (Period)
\101 A -- Latin Letter Capital 'A'
\172 z -- Latin Letter Small 'z'
\316\221 Α -- Greek Letter Capital Alpha
\317\211 ω -- Greek Letter Small Omega
\320\251 Щ -- Cyrillic Capital Letter Shcha
\340\244\204 ऄ -- Devanagari Letter Short A
\341\232\240 ᚠ -- Runic Letter Fehu Feoh Fe F
\342\235\244 ❤ -- Heavy Black Heart
\346\227\245 日 -- CJK Ideographs Sun
\360\220\202\205 𐂅 -- Linear B Ideogram B105M Stallion
\360\235\215\261 𝍱 -- Counting Rod Tens Digit Nine
\364\217\277\260 􏿰 -- Unicode Code Point U+10FFF0
